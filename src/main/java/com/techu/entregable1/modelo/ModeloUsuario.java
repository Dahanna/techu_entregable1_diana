package com.techu.entregable1.modelo;

public class ModeloUsuario {
    private int id;
    private String nombre;

    public  ModeloUsuario(int id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }
    public  void setNombre(String nombre){
        this.nombre = nombre;
    }
    public String getNombre (){
        return this.nombre;
    }
    public void setId(int id){
        this.id = id;
    }
    public int getId() {
        return  this.id;
    }
}

