package com.techu.entregable1.controlador;

import com.techu.entregable1.modelo.ModeloProducto;
import com.techu.entregable1.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/productos")
public class ControladorProducto {

    @Autowired
    private ServicioDatos servicioDatos;

    //GET todos los productos
    @GetMapping("/")
    public ResponseEntity obtenerProductos(){
        return ResponseEntity.ok(this.servicioDatos.obtenerProductos());
    }

    // GET producto por id
    @GetMapping("/{id}")
    public ResponseEntity obtenerUnProducto(@PathVariable int id) {
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if(p == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(p);
    }

    //POST
    @PostMapping("/")
    public ResponseEntity crearProducto(@RequestBody ModeloProducto producto){
        final ModeloProducto p = this.servicioDatos.agregarProducto(producto);
        return new ResponseEntity<>("Producto created successfully", HttpStatus.CREATED);
    }


    //PUT
    @PutMapping("/{id}")
    public ResponseEntity actualizarProducto(@PathVariable int id, @RequestBody ModeloProducto productoAActualizar){
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if (p == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        servicioDatos.actualizarProducto(id-1, productoAActualizar);
        return new ResponseEntity<>("Producto actualizado correctamente.",HttpStatus.OK);
    }

    //DELETE
    @DeleteMapping("/{id}")
    public ResponseEntity borrarProducto(@PathVariable int id){
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if (p == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        servicioDatos.borrarProducto(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    //PATCH
    @PatchMapping("/{id}")
    public ResponseEntity patchPrecioProducto(@PathVariable int id, @RequestBody ModeloProducto modeloProducto){
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if (p == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
       p.setPrecio(modeloProducto.getPrecio());
        servicioDatos.actualizarProducto(id, p);
        return new ResponseEntity<>(p, HttpStatus.OK);
    }

    //GET subrecurso
    @GetMapping("/{id}/usuarios")
    public ResponseEntity obtenerProductoUsuario(@PathVariable int id){
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if (p == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (p.getUsuarios()!= null)
            return ResponseEntity.ok(p.getUsuarios());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
