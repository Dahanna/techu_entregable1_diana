package com.techu.entregable1.servicio;

import com.techu.entregable1.modelo.ModeloProducto;
import com.techu.entregable1.modelo.ModeloUsuario;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


// CRUD de Productos
// Create
// Read
//Update
//Delete

@Component
public class ServicioDatos {
    private final AtomicInteger secuenciaIdsProductos
            = new AtomicInteger(0);
    private final AtomicInteger secuenciaIdsUsuarios
            = new AtomicInteger(0);
    private final List<ModeloProducto> productos
            = new ArrayList<ModeloProducto>();

    //Create
    public ModeloProducto agregarProducto(ModeloProducto producto){
        producto.setId(this.secuenciaIdsProductos.incrementAndGet());
        this.productos.add(producto);
        return producto;
    }

    public ModeloUsuario agregarUsuarioProducto(int idProducto, ModeloUsuario usuario){
        usuario.setId(this.secuenciaIdsUsuarios.incrementAndGet());
        this.obtenerProductoPorId(idProducto).getUsuarios().add(usuario);
        return usuario;
    }

    //Read coleccion
    public List<ModeloProducto> obtenerProductos(){
        return Collections.unmodifiableList(this.productos);
    }

    public List<ModeloUsuario> obtenerUsuariosProducto(int idProducto){
        return Collections.unmodifiableList(this.obtenerProductoPorId(idProducto).getUsuarios());
    }

    //Read elemento
    public ModeloProducto obtenerProductoPorId(int id){
        for (ModeloProducto p: this.productos){
            if (p.getId() == id)
                return p;
        }
        return null;
    }

    public ModeloUsuario obtenerUsuarioProducto(int idProducto, int idUsuario){
        for (ModeloUsuario u: this.obtenerProductoPorId(idProducto).getUsuarios()){
            if (u.getId() == idUsuario){
                return u;
            }
        }
        return null;
    }

    //UPDATE
    public boolean actualizarProducto(int id, ModeloProducto producto){
        for (int i = 0; i < this.productos.size();++i){
            if (this.productos.get(i).getId() == id){
                producto.setId(id);
                this.productos.set(i, producto);
                return true;
            }
        }
        return false;
    }

    public boolean actualizarUsuarioProducto(int idProducto, int idUsuario, ModeloUsuario usuario){
        List<ModeloUsuario> usuarios = this.obtenerProductoPorId(idProducto).getUsuarios();
        for (int i = 0; i < usuarios.size(); ++i){
            if (usuarios.get(i).getId() == idUsuario){
                usuario.setId(idUsuario);
                usuarios.set(i, usuario);
                return true;
            }
        }
        return false;
    }

    //DELETE
    public boolean borrarProducto(int id) {
        for (int i = 0; i < this.productos.size();++i){
            if (this.productos.get(i).getId() == id){
                this.productos.remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean borrarUsuarioProducto(int idProducto, int idUsuario){
        List<ModeloUsuario> usuarios = this.obtenerProductoPorId(idProducto).getUsuarios();
        for (int i = 0; i < usuarios.size(); ++i){
            if (usuarios.get(i).getId() == idUsuario){
                usuarios.remove(i);
                return true;
            }
        }
        return false;
    }
}
